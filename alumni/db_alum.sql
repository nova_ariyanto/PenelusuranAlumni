-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2018 at 04:37 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_alum`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_alumni`
--

CREATE TABLE `tb_alumni` (
  `id_alumni` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `nis` int(11) DEFAULT NULL,
  `tempat_lahir` char(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `tahun_lulus` int(11) DEFAULT NULL,
  `id_jurusan` int(11) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `no_hp` varchar(16) DEFAULT NULL,
  `fb` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `instagram` varchar(100) DEFAULT NULL,
  `twiter` varchar(100) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_alumni`
--

INSERT INTO `tb_alumni` (`id_alumni`, `nama`, `nis`, `tempat_lahir`, `tanggal_lahir`, `tahun_lulus`, `id_jurusan`, `foto`, `no_hp`, `fb`, `alamat`, `instagram`, `twiter`, `status`) VALUES
(142, 'nova', 1000, 'rembang', '2018-01-01', 2015, 3, 'lontong.jpg', 'asdf', 'sf', 'sadf', 'dsf', 'sdf', 2),
(145, 'maman', 102, 'rembang', '2018-01-02', 2015, 1, 'kg2.jpg', 'safd', 'sdf', 'rembang', 'sdfs', 'sdf', 2),
(149, 'Mimin', 1200, 'rembang', '2018-01-01', 2014, 4, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(150, 'momo', 123, 'rembang', '2018-01-01', 2016, 3, 'kominfo.jpg', '0182391', 'momo', '2012', 'momo', 'momo', 2),
(151, 'tete', 123, 'rembang', '2018-01-01', 2012, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(152, 'tete', 123, 'rembang', '2018-01-01', 2015, 4, 'kominfo.jpg', '1923', 'kjfaas', 'rembang', 'k', 'k', 2),
(153, 'tete', 123, 'rembang', '2018-01-01', 2015, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(154, 'momo', 123, 'rembang', '2018-01-01', 2016, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_jurusan`
--

CREATE TABLE `tb_jurusan` (
  `id_jurusan` int(11) NOT NULL,
  `nama_jurusan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jurusan`
--

INSERT INTO `tb_jurusan` (`id_jurusan`, `nama_jurusan`) VALUES
(1, 'Teknik Kendaraan Ringan1'),
(3, 'Teknik Kendaraan Ringan'),
(4, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `tb_status_pekerjaan`
--

CREATE TABLE `tb_status_pekerjaan` (
  `id_sp` int(11) NOT NULL,
  `nama_perusahaan` varchar(100) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `alamat_perusahaan` varchar(100) NOT NULL,
  `tahun` int(4) NOT NULL,
  `id_alumni` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_status_pekerjaan`
--

INSERT INTO `tb_status_pekerjaan` (`id_sp`, `nama_perusahaan`, `jabatan`, `alamat_perusahaan`, `tahun`, `id_alumni`) VALUES
(1, 'PT. Kejar Koding', 'CEO', 'Rembang', 2018, 142),
(2, 'Kejar', 'jb', 'ap', 2020, 142),
(5, 'Kejar Koding ', 'Founder', 'Rembang', 2012, 145),
(6, 'PT. Nusu', 'CEO', 'Rembang', 2012, 150);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` int(11) NOT NULL,
  `id_pengguna` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `role`, `id_pengguna`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 0, 1),
(9, 'maman', '33437d53bf1287344672c061a4338e38', 2, 145, 1),
(10, 'tete', '2576c96285e66bb8420c4278cad2b186', 2, 152, 1),
(11, 'momo', '2576c96285e66bb8420c4278cad2b186', 2, 150, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_alumni`
--
ALTER TABLE `tb_alumni`
  ADD PRIMARY KEY (`id_alumni`);

--
-- Indexes for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `tb_status_pekerjaan`
--
ALTER TABLE `tb_status_pekerjaan`
  ADD PRIMARY KEY (`id_sp`),
  ADD KEY `id_alumni` (`id_alumni`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_alumni`
--
ALTER TABLE `tb_alumni`
  MODIFY `id_alumni` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=158;

--
-- AUTO_INCREMENT for table `tb_jurusan`
--
ALTER TABLE `tb_jurusan`
  MODIFY `id_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_status_pekerjaan`
--
ALTER TABLE `tb_status_pekerjaan`
  MODIFY `id_sp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
