
<!DOCTYPE html>
<html>
<head>
	<title>Data Proyek</title>
</head>
<body>
	<div class="col-md-8">
	<h4> <span class="glyphicon glyphicon-"></span> Data Alumni</h4>
	<!-- Trigger the modal with a button -->
					<a href="?pages=data_alumni&act=tambah" class="btn btn-sm btn-success"> <span class="glyphicon glyphicon-add"></span> Tambah Data Alumni</a>
					<a href="" class="btn btn-sm btn-info"> <span class="glyphicon glyphicon-refresh"></span> Perbarui	</a>
	</div>
<div class="col-md-4">
		<form method="post" action="">
		 <div class="input-group">
      <input type="text" class="form-control" name="inputan" placeholder="Cari data..">
      <span class="input-group-btn">
        <button class="btn btn-success" type="submit" name="cari"><span class="glyphicon glyphicon-search"></span></button>
      </span>
      </div>
      </form>
 <br>
    	Jumlah Data  <span class="label label-success"><?php 
    	$qjumlah = mysqli_query($koneksi,"SELECT * FROM tb_alumni");	
    	$jumlah_data = mysqli_num_rows($qjumlah);
    	echo $jumlah_data; ?></span> 
    </div>

    <!-- /input-group -->
    	<div class="col-md-12">
    	<br/>
    	<center>
	<table class="table table-striped" style="text-align: left;">
			<thead>
				<tr class="bg-success">
					<th>No.</th>
					<th>Nama</th>
					<th>Nis</th>
					<th>Tahun Lulus</th>
					<th>Jurusan</th>
					<th>Status User</th>
					<th>Aksi</th>
				</tr>
			</thead>	
			<tbody>
				<tr>
							<?php
						$batas = 7;
						$hal = ceil($jumlah_data / $batas);
						$page = (isset($_GET['hal'])) ? $_GET['hal']:1;
						$posisi = ($page - 1) * $batas;

						$inputan = @$_POST['inputan']; 
						if(isset($_POST['cari'])){
							if($inputan != ""){
								$qtampil = mysqli_query($koneksi,"SELECT *  FROM tb_alumni 
									INNER JOIN tb_jurusan on tb_alumni.id_jurusan = tb_jurusan.id_jurusan 
									WHERE tb_alumni.nama 
									LIKE '%$inputan%' ORDER BY tb_alumni.id_alumni DESC");
							}
							else if($inputan==""){
								$qtampil = mysqli_query($koneksi,"SELECT *  FROM tb_alumni 
									INNER JOIN tb_jurusan on tb_alumni.id_jurusan = tb_jurusan.id_jurusan 
									WHERE tb_alumni.nama 
									LIKE '%$inputan%' ORDER BY tb_alumni.id_alumni DESC
									LIMIT $posisi,$batas ");
							}
						} else {
							$qtampil = mysqli_query($koneksi,"SELECT *  FROM tb_alumni 
									INNER JOIN tb_jurusan on tb_alumni.id_jurusan = tb_jurusan.id_jurusan 
									WHERE tb_alumni.nama 
									LIKE '%$inputan%' ORDER BY tb_alumni.id_alumni DESC
							 LIMIT $posisi,$batas");
						}
						$cek = mysqli_num_rows($qtampil);

						if($cek <= 0){
							?>
								<tr>
									<td colspan="12"> <center>Data tidak ada ! <a href="" class="btn btn-success">refresh</a></center></td>
								</tr>
							<?php
							echo "<center><h4><small>Hasil cari dari</small> :".$inputan."</h4></center>";
						} else {

						$no=1+$posisi;
						while($d = mysqli_fetch_object($qtampil)){
					 ?>
				<tr>
					<td><?php echo $no++; ?>.</td>	
					<td><?php echo $d->nama; ?></td>
					<td><?php echo $d->nis; ?></td>
					<td><?php echo $d->tahun_lulus; ?></td>
					<td><?php echo $d->nama_jurusan; ?></td>	
					<td>
						<?php
						if($d->status == 1){
							echo "menunggu konfirmasi";
							?>
							<a href="?pages=data_alumni&act=konfirmasi_alumni&id_alumni=<?php echo $d->id_alumni;?>" class="btn btn-success btn-xs">Aktifkan</a>
							<?php
						}elseif($d->status == 2){
							echo "Aktif";
						}else{
							echo "Belum terdaftar";
						}
						?>
					</td>
					
					<td>
						<a href="?pages=proyek&act=edit&id_project=<?php echo $d->id_project; ?>" class="btn btn-success btn-xs">Detail</a>
						<a Onclick="return confirm('Alumni dengan nama <?php echo $d->nama; ?> Akan anda hapus ?');" <a href="?pages=data_alumni&act=hapus&id_alumni=<?php echo $d->id_alumni; ?>" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-trash"></span>	</a>		
						<a href="?pages=data_alumni&act=ubah&id_alumni=<?php echo $d->id_alumni; ?>" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-edit"></span>	</a>
					</td>
				</tr>
				<?php } } ?>
				</tr>

			</tbody>
			</table
			<nav>
  <ul class="pagination">
    <li>
    	<?php 
    		if($page!="1"){
    			$pri = $page-1;	
    		}else if($page == "1"){
    			$pri = $page-0;
    		}
    	?>
      <a href="?pages=data_alumni&hal=<?php echo $pri; ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    		
    </li>
    <?php 
    	for($i=1; $i<=$hal; $i++){
    ?>
    <li <?php if($i==$page){echo "class='active'";} ?>><a href="?pages=proyek&hal=<?php echo $i; ?>"><?php echo $i; ?>	</a></li>
    <?php } ?>
    <li>
      <?php 
      if($page!=$hal){
      $next = $page+1; 
    	}else {
    		$next=$page+0;
    	}
      ?>
      <a href="?pages=proyek&hal=<?php echo $next; ?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
		</center>
	</body>
</html> 