
<!DOCTYPE html>
<html>
<head>
    <title>Selamat Datang !</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <script type="text/javascript" src="../js/jquery.min.js"></script> 
    <script src="../assets/js/ie-emulation-modes-warning.js"></script>

    <link href="../css/style.css" rel="stylesheet">
    <style type="text/css">
        #input {
            width: 80%;

        }
    </style>
    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>

<?php
include '../inc/koneksi.php';
session_start();
$id_alumni = @$_SESSION['id_alumni'];
$qry = mysqli_query($koneksi,"select * from tb_alumni where id_alumni='$id_alumni'")or die(mysqli_error());
$d = mysqli_fetch_object($qry);
    if(mysqli_num_rows($qry)==1){
        
        if($d->status == 1){
            ?>
             <div class="col-md-5 col-md-offset-3" style="margin-top:60pt ">
                <p style="font-size: 3em;text-align: center;">Sistem Penelusuran Alumni</p>
                <p style="text-align: center;font-size: 10pt">Tunggu admin mengkonfirmasi akunmu ,<i>untuk login gunakan <b style="color: red;">NIS</b> sebagai username dan tanggal lahir sebagai <b style="color: red;">password</b> anda</p>
                <br>
            </div>
            <?php
        }elseif($d->status==2){
          header('location:signin.php');
        }else{
?>
<div class="row">
    <form method="post" action="">      
    <div class="col-md-5 col-md-offset-3" style="margin-top:60pt ">
                <p style="font-size: 3em;text-align: center;">Sistem Penelusuran Alumni
                </p>
                <h4 style="text-align: center;">Selamat Datang <?php echo $d->nama;?></h4> 
                <br/>
                <br/>
                <table class="table table-default">
                    <tr>
                        <td>Nama</td>
                        <td><?php echo $d->nama;?></td>
                    </tr>
                    <tr>
                        <td>Tempat/Tgl Lahir</td>
                        <td><?php echo $d->tempat_lahir." / ".$d->tanggal_lahir;?></td>
                    </tr>
                </table>
                <p style="text-align: center">Agar bisa login dan melengkapi datamu, klik daftar dan tunggu admin memberi konfirmasi status akunmu.</p>
                <input type="submit" name="daftar" value="daftar" class="btn btn-primary">
                <?php 
                    if(isset($_POST['daftar'])){
                        $qry = mysqli_query($koneksi,"UPDATE `tb_alumni` SET `status` = '1' WHERE `tb_alumni`.`id_alumni` = '$id_alumni' ;");
                        echo $id_alumni;
                    }
                ?>
    </form>
</div>  
<?php
        }

    }
 ?>
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>