<?php
include '../inc/koneksi.php';
session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Selamat Datang !</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <script type="text/javascript" src="../js/jquery.min.js"></script> 
    <script src="../assets/js/ie-emulation-modes-warning.js"></script>

    <link href="../css/style.css" rel="stylesheet">
    <style type="text/css">
    	#input {
    		width: 80%;

    	}
    </style>
    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>
<div class="row">
	<form method="post" action="">		
	<div class="col-md-5 col-md-offset-3" style="margin-top:60pt ">
				<p style="font-size: 3em;text-align: center;">Sistem Penelusuran Alumni
				</p>
				<h4 style="text-align: center;">Cari datamu dengan memasukkan data berikut</h4>	
				<br/>
				<br/>
				<br/>
				<div class="form-group">
    				<label>Nis</label>
    				<input type="number" name="nis" required class="form-control" value="">
    			</div>
    			<div class="form-group">
    				<label>Tahun lulus</label>
    				<input type="number" name="tahun_lulus" required class="form-control" value="">
    			</div>
    			<div class="form-group">
    				<label>Jurusan</label>
    					<select class="form-control" name="id_jurusan" required="">
    						<?php
 							$query = mysqli_query($koneksi,"SELECT *  FROM tb_jurusan order by id_jurusan ASC ")or die(mysqli_error($koneksi));
							?>
    						<option value="">Pilih jurusan</option>
    					<?php while($d = mysqli_fetch_object($query)){
		    				?>		    	
		    		<option value="<?php echo $d->id_jurusan ?>"><?php echo $d->nama_jurusan ?></option>
		    			<?php }?>
    					</select>
    			</div>
    			<input type="submit" name="temukan" class=" btn btn-primary" value="Temukan">	
    	<?php 
					$nis= @$_POST['nis'];
					$tahun_lulus= @$_POST['tahun_lulus'];
					$id_jurusan = @$_POST['id_jurusan'];

					if(isset($_POST['temukan'])){
						$qry = mysqli_query($koneksi,"select * from tb_alumni where nis = '$nis' and tahun_lulus='$tahun_lulus' and id_jurusan = '$id_jurusan'")or die(mysqli_error());
						$d = mysqli_fetch_object($qry);
							if(mysqli_num_rows($qry)==1){
								$_SESSION['id_alumni'] = $d->id_alumni;
								header('location:register.php');
							}else{
								 ?>
            <br/>
            <br>
           <div class="alert alert-danger">
                 Nis,Tahun Lulus dan  jurusan tidak cocok .
           </div>
           <?php
							}
					}
			  ?>
	</form>
</div>		
    <script src="../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>