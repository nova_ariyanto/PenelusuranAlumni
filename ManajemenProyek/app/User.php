<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='tb_user';
    protected $primaryKey = 'id_user';
    public function level(){
    	return $this->belongsTo('App\Level','id_level');
    }

}
