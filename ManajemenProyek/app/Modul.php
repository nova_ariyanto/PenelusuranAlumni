<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modul extends Model
{
    //
    protected $table= 'tb_modul';
    protected $primaryKey ='id_modul';
    public $timestamps = false;
}
