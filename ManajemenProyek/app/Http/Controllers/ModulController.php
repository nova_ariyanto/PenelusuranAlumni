<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modul;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Redirect;
use Session;

class ModulController extends Controller
{

    private $folder = 'modul';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {


        // // Untuk login

        // var_dump(session('userid'));
             $this->middleware('login-auth');

        // if (session('userid')==null) {


        //     Redirect::to('login')->send();

        // }
    }

    public function data(){
        $model = Modul::query();
            return Datatables::of($model)->addColumn('action',function($data){
                $html ='';
                $html .= '<a href="'.url('modul/'.$data->id_modul.'/edit').'" class="btn btn-success">Edit</a> ';
                $html .= '<form action="'.url('modul/'.$data->id_modul).'" method="post" style="display:inline">';
                $html .= csrf_field();
                $html .= method_field("DELETE");
                $html .= "<button class='btn btn-danger' type='submit'>Hapus</button>";
                $html .= "</form>";
                return $html;
            })
            ->make(true);
    }
    public function index()
    {
        //


        $data = Modul::paginate(10);
        // echo count($data);

        return view($this->folder.'/index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view($this->folder.'/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new Modul;
        $data->nama= $request->modul;
        $data->status = 0;
        $save = $data->save();
        if ($save) {
            $request->session()->flash('success','Berhasil Menyimpan Data');
        }
        else{
            $request->session()->flash('success','Gagal Menyimpan Data');

        }
        return redirect('modul');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Modul::find($id);
        return view($this->folder.'/edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Modul::find($id);
        if (count($data) > 0) {
        $data->nama = $request->modul;
        $save = $data->save();
        if ($save) {
            $request->session()->flash('success','Berhasil Mengubah Data');
        }
        else{
            $request->session()->flash('danger','Gagal Mengubah Data');

        }
        return redirect('/modul');

        }
        else{
            $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        //
        $data = Modul::find($id);
        if (count($data) > 0) {
        $delete = $data->delete();
           if ($delete) {
            $request->session()->flash('success','Berhasil Menghapus Data');
        }
        else{
            $request->session()->flash('danger','Gagal Menghapus Data');

        }
        return redirect('/modul');
        }
        else{

            $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }
}
