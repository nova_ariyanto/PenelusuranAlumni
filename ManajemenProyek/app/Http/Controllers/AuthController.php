<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{
    //
    public function login(Request $request){
    	return view('auth/login');
    }

    public function loginPost(Request $request){
    	$username = $request->username;
    	$password = $request->password;
    	$user = User::where(['username'=>$username,'password'=>md5($password)]);

    	if ($user->count() == 1) {
    		// $request->session();
    		session(['levelid'=>$user->first()->id_level,'userid'=>$user->first()->id_user,'username'=>$user->first()->username]);
    		return redirect('/');

    		// print_r(session('userid'));
    		// print_r($request->session('userid'));
    	}
    	else{
    		$request->session()->flash('danger','Username/Password Salah');
    		return redirect()->back();
    	}

    }
    public function logoutPost(Request $request){
    	$request->session()->flush();

    	$request->session()->flash('success','Silahkan login');
    	return redirect('/login');
    }
}
