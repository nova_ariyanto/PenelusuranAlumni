<?php

namespace App\Http\Controllers;
use App\Level;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class LevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $folder ='level';

   
   public function __construct()
   {
       // parent::__construct();
                    $this->middleware('login-auth');
   }
    public function data(){
        $level = Level::query();
            return Datatables::of($level)->addColumn('action',function($data){
                $html ='';
                $html .= '<a href="'.url('level/'.$data->id_level.'/edit').'" class="btn btn-success">Edit</a> ';
                $html .= '<form action="'.url('level/'.$data->id_level).'" method="post" style="display:inline">';
                $html .= csrf_field();
                $html .= method_field("DELETE");
                $html .= "<button class='btn btn-danger' type='submit'>Hapus</button>";
                $html .= "</form>";
                return $html;
            })
            ->make(true);
    }


    public function index()
    {
        //
        $data = Level::paginate(10);
        return view($this->folder.'/index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view($this->folder.'/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new Level;
        $data->nama = $request->nama;
        // $data->status = 0;
        $save = $data->save();
        if($save){
            $request->session()->flash('success','Berhasil Menyimpan Data');
        }else{
            $request->session()->flash('success','Gagal Menyimpan Data');
        }
        return redirect('level');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Level::find($id);
        return view($this->folder.'/edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data  = Level::find($id);
        if($data->count()>0){
            $data->nama = $request->nama;
            $save = $data->save();
             if ($save) {
            $request->session()->flash('success','Berhasil Mengubah Data');
        }
        else{
            $request->session()->flash('danger','Gagal Mengubah Data');

        }
        return redirect('/level');
        }else{
           $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id ,Request $request)
    {
        //
        $data = Level::find($id);
        if ($data->count() > 0) {
        $delete = $data->delete();
           if ($delete) {
            $request->session()->flash('success','Berhasil Menghapus Data');
        }
        else{
            $request->session()->flash('danger','Gagal Menghapus Data');

        }
        return redirect('/level');
        }else{

            $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }

    }
}
