<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelUser;
use App\Level;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $title = 'Data User';
    private $folder ='user';    


    public function __construct()
    {
        // parent::__construct();
                     $this->middleware('login-auth');
    }
    public function data(){
        $level = session('idlevel');
        if($level == 1){
            $user = ModelUser::query();
        }else{
            $user = ModelUser::Where('id_level','>','1')->get();
        }
      return Datatables::of($user)
            ->addColumn('Level',function($data){

                $level = Level::find($data->id_level);
                $html = '';
                $html .= $level->nama;
                
                return $html;
            })
            ->addColumn('action',function($data){
                $html ='';
                $html .= '<a href="'.url('user/'.$data->id_user.'/edit').'" class="btn btn-success">Edit</a> ';
                $html .= '<form action="'.url('user/'.$data->id_user).'" method="post" style="display:inline">';
                $html .= csrf_field();
                $html .= method_field("DELETE");
                $html .= "<button class='btn btn-danger' type='submit'>Hapus</button>";
                $html .= "</form>";
                return $html;
            })
            ->make(true);
    }

    public function index()
    {
        //
        $data = ModelUser::all();
        // $title = 'Data User';
        $no = 1;
        $title = $this->title;
        $page = "Data User";

        return view('user/index',compact('data','no','title','page'));
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $level = session('idlevel');
        if($level == 1){
            $level = Level::all();
        }else{
            $level = Level::Where('id_level','>','1')->get();
        }
        $title = $this->title;
        $page = "Tambah Data Level";
        return view($this->folder.'/create',compact('page','title','level'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new ModelUser;
        $data->nama = $request->nama;
        $data->id_level = $request->id_level;
        $data->username = $request->username;
        $data->password = md5($request->password);

        $save = $data->save();
         if ($save) {
            $request->session()->flash('success','Data Berhasil Disimpan');
        }
        else{
            $request->session()->flash('status','Data Gagal Disimpan');   
        }

        return redirect('/user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
            $level = session('idlevel');
        if($level == 1){
            $level = Level::all();
        }else{
            $level = Level::Where('id_level','>','1')->get();
        }
        $data = ModelUser::find($id);
        return view($this->folder.'/edit',compact('data','level'));
     
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = ModelUser::find($id);
        if(count($data)>0){
            if(empty($request->password)){

            
        $data->nama = $request->nama;
        $data->id_level = $request->id_level;
        $data->username = $request->username;
         $save = $data->save();
          if ($save) {
            $request->session()->flash('success','Berhasil Mengubah Data');
        }
        else{
            $request->session()->flash('danger','Gagal Mengubah Data');

        }
        return redirect('/user');        
        }else{

        $data->nama = $request->nama;
        $data->id_level = $request->id_level;
        $data->username = $request->username;
        $data->password = md5($request->password);
         $data->username = $request->username;
         $save = $data->save();
          if ($save) {
            $request->session()->flash('success','Berhasil Mengubah Data');
        }
        else{
            $request->session()->flash('danger','Gagal Mengubah Data');

        }
        return redirect('/user');        

        }


        }else{
               $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id ,Request $request)
    {
        //
        $data = ModelUser::find($id);
        if (count($data) > 0) {
        $delete = $data->delete();
           if ($delete) {
            $request->session()->flash('success','Berhasil Menghapus Data');
        }
        else{
            $request->session()->flash('danger','Gagal Menghapus Data');

        }
        return redirect('/user');
        }
        else{

            $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }
}
