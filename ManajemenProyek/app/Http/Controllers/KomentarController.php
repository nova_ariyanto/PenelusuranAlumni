<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modul;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\Komentar;

class KomentarController extends Controller
{

    private $folder = 'modul';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function save($requestId,Request $request){
        $text = $request->komentar;
        $userid = session('userid');

        $komentar = new Komentar;
        $komentar->komentar = $text;
        $komentar->id_request = $requestId;
        $komentar->id_user = $userid;
        $save = $komentar->save();

        if ($save) {
            return redirect()->back();
        }

    }

    public function delete($id,Request $request){
        $userid = session('userid');

        $komentar = Komentar::find($id);
        if (count($komentar) != 0) {
            $userIdKomentar = $komentar->id_user;

            if ($userid == $userIdKomentar) {
            $delete = $komentar->delete();
            if ($delete) {
                $request->session()->flash('success','Berhasil Menghapus');
            }
            else{
                $request->session()->flash('danger','Gagal Menghapus');

            }

            }
            else{

                $request->session()->flash('danger','Anda Tidak Bisa Menghapus');                
            }

        }
        else{

                $request->session()->flash('danger','Berhasil Menghapus');
        }

        // print_r(session('danger'));


        return redirect()->back();
    }


}
