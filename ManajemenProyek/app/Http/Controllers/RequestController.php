<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestData as Data;
use Session;
use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Redirect;
use App\Kategori;
use App\Modul;
use App\RequestModul;
use App\Komentar;
use App\Disposisi;

class RequestController extends Controller
{
    private $folder = 'request';
    

    public function __construct()
    {

        // if (session('userid')==null) {


        //     Redirect::to('login')->send();

        // }
                     $this->middleware('login-auth');
    }

        public function data(){
        $model = Data::query();
            return Datatables::of($model)
                ->addColumn('status',function($data){
                    $html ='';
                    $html .= $data->status == 0 ? "Ditinjau" : ($data->status==1 ? 'Dikerjakan':"Selesai");
                    return  $html;
                })
                ->addColumn('action',function($data){
                $html ='';
                $html .= '<a href="'.url('request/'.$data->id_request).'" class="btn btn-info">Detail</a> ';
                $html .= '<a href="'.url('request/'.$data->id_request.'/edit').'" class="btn btn-success">Edit</a> ';
                $html .= '<form action="'.url('request/'.$data->id_request).'" method="post" style="display:inline">';
                $html .= csrf_field();
                $html .= method_field("DELETE");
                $html .= "<button class='btn btn-danger' type='submit'>Hapus</button>";
                $html .= "</form>";
                return $html;
            })
            ->make(true);
    }

    public function index()
    {
        //

        $levelid = session('levelid');
        $userid = session('userid');

        if ($levelid == 1 || $levelid == 2) {
        $data = Data::paginate(10);
        return view($this->folder.'/index',compact('data'));
        }
        else{

        $data = Data::where('id_user',$userid)->paginate(10);
        return view($this->folder.'/data',compact('data'));
        }
        // echo count($data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategori = Kategori::all();
        $modul = Modul::all();
        return view($this->folder.'/create',compact('kategori','kategori','modul'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $nama = $request->nama;
        $kategori = $request->kategori;
        $deskripsi = $request->deskripsi;

        $modul = $request->modul;
            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $c = unpack("C*",$charid);
            $c = implode("",$c);
            $id =  substr($c,0,10);

        // cek duplikasi id
            $data = Data::find($id);
            if (count($data) == NULL) {
                $id = $id;
            }
            else{

            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $c = unpack("C*",$charid);
            $c = implode("",$c);
            $uniqueid=  substr($c,0,10);

                $id = $uniqueid;
            }

        // // print_r($modul);
        $data = new Data;
        $data->id_request = $id;
        $data->nama_request = $nama;
        $data->id_kategori = $kategori;
        $data->deskripsi = $deskripsi;
        $data->status = 0;
        $data->id_user = session('userid');
        $save = $data->save();


        if (count($modul) != 0) {

        foreach ($modul as $m) {
        $requestModul = new RequestModul;
        $requestModul->id_request = $id;
        $requestModul->id_modul = $m;
        $requestModul->nama = NULL;
        $requestModul->status = NULL;
        $requestModul->save();

        }

        }

        if ($save) {
            $request->session()->flash('success','Berhasil Menyimpan Data');
        }
        else{
            $request->session()->flash('success','Gagal Menyimpan Data');

        }
        return redirect('request');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
        //
        // var_dump($request);
        // print_r($request);
        $data = Data::find($id);
        if (count($data) > 0) {
        $modul = RequestModul::where('id_request',$id);
        $disposisi = Disposisi::where('id_request',$id)->count();
        $komentar = Komentar::where('id_request',$id);
        return view($this->folder.'/show',compact('data','modul','komentar','disposisi'));
        }
        else{
            $request->session()->flash('danger','Maaf Data Tidak Ada');
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Data::find($id);
        $requestModul = RequestModul::where('id_request',$id);

        $kategori = Kategori::all();
        $modul = Modul::all();
        // return $data;
        $array = [];
        foreach ($modul as $m) {
            $state = RequestModul::where('id_modul',$m->id_modul);
            $array[] = [
            'id'=>$m->id_modul,
            'state'=>$state->count() > 0 ? 'selected' : '',
            'modul'=>$m->nama
            ];
        }

        return view($this->folder.'/edit',compact('data','requestModul','kategori','modul','array'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $nama = $request->nama;
        $kategori = $request->kategori;
        $deskripsi = $request->deskripsi;

        $modul = $request->modul;



        // // print_r($modul);
        $data = Data::find($id);
        $data->nama_request = $nama;
        $data->id_kategori = $kategori;
        $data->deskripsi = $deskripsi;
        $data->status = 0;
        $save = $data->save();


        if (count($modul) != 0) {
        $requestModul = RequestModul::where('id_request',$id)->delete();
        foreach ($modul as $m) {
        $requestModul = new RequestModul;
        $requestModul->id_request = $id;
        $requestModul->id_modul = $m;
        $requestModul->nama = NULL;
        $requestModul->status = NULL;
        $requestModul->save();

        }

        }

        if ($save) {
            $request->session()->flash('success','Berhasil Menyimpan Data');
        }
        else{
            $request->session()->flash('success','Gagal Menyimpan Data');

        }
        return redirect('request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        //
        $data = Data::find($id);
        if (count($data) > 0) {
        $delete = $data->delete();
           if ($delete) {
            $request->session()->flash('success','Berhasil Menghapus Data');
        }
        else{
            $request->session()->flash('danger','Gagal Menghapus Data');

        }
        return redirect('/request');
        }
        else{

            $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }

    public function change($id,Request $request){
        $status = $request->status;

        $data = Data::find($id);
        $data->status = $status;
        $update = $data->save();
        if($update){
            return redirect()->back();
        }
    }
}
