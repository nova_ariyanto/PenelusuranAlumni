<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modul;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\User;
use App\Disposisi;
use App\DisposisiUser;
use App\RequestData as Data;
use App\RequestModul;
use App\Level;
// use App\DisposisiUser;
class DisposisiController extends Controller
{

    private $folder = 'modul';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
    {


        // // Untuk login

        // var_dump(session('userid'));
             $this->middleware('login-auth');
    }

    public function create($requestid){

        $user = User::where('id_level','4')->get();
        return view('disposisi/create',compact('requestid','user'));
    }
    public function save(Request $request){

            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $c = unpack("C*",$charid);
            $c = implode("",$c);
            $id =  substr($c,0,10);

        // cek duplikasi id
            $data = Disposisi::find($id);
            if (count($data) == NULL) {
                $uniqueid = $id;
            }
            else{

            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $c = unpack("C*",$charid);
            $c = implode("",$c);
            $uniqueid=  substr($c,0,10);

                $uniqueid = $uniqueid;
            }



    $id  = $request->id;
    $tgl = $request->tgl;
    $waktu = $request->waktu;
    $users = $request->users;
    $users  = explode(',',$users);
    $tugas =  explode(',',$request->tugas);
    $deskripsi =  explode(',',$request->deskripsi);


    $disposisi = new Disposisi;
    $disposisi->id_disposisi = $uniqueid;
    $disposisi->id_request = $id;
    $disposisi->id_user_it = session('userid');
    $disposisi->tanggal_mulai = $tgl;
    $disposisi->waktu_deadline = $waktu;
    $disposisi->status_pengerjaan = 0;
    $disposisi->save();

    for ($i=1; $i < count($users) ; $i++) { 
        $tugasData = $tugas[$i];
        $userData = $users[$i];
        $deskripsiData = $deskripsi[$i];

        $disposisiUser = new DisposisiUser;
        $disposisiUser->id_disposisi =  $uniqueid;
        $disposisiUser->id_user_support= $userData;
        $disposisiUser->tugas = $tugasData;
        $disposisiUser->status_pengerjaan = 0;
        $disposisiUser->keterangan = $deskripsiData;

        $disposisiUser->save();


        // echo $deskripsi;
        // echo $i;


    }

    $response = ['status'=>'ok','id'=>$id];

    return $response;

    // echo ;


    }

    public function show($requestid){
        $level = session('idlevel');
        if($level == 3){
            $level = Level::all();
        }else{
            $user = Level::Where('id_level','>','1')->get();
        }

        $data = Disposisi::where('id_request',$requestid);
        if ($data->count() > 0) {
        // echo $request;
        $id = $data->first()->id_disposisi;
        $disposisiUser = DisposisiUser::where('id_disposisi',$id);
        $request = Data::find($requestid);
        $modul = RequestModul::where('id_request',$requestid);

        $user = User::where('id_level','4')->get();

        return view('disposisi/show',compact('data','disposisiUser','request','modul','user'));
        // return $disposisiUser;

        }
        else{
            return abort(404);
        }
    }

    public function post(Request $request,$id){
        $type = $request->type;
        $val = $request->val;

        if ($type=='ubah-waktu') {
            $data = ['waktu_deadline'=>$val];
        }
        elseif($type == 'ubah-tanggal'){
            $data = ['tanggal_mulai'=>$val];

        }
        $save = $disposisi = Disposisi::where('id_request',$id)->update($data);
        if ($save) {
            return [
            'status'=>'ok'
            ];
        }

    }

    public function addUser($id,Request $request){

        $data = Disposisi::where('id_request',$id);
        $id_disposisi = $data->first()->id_disposisi;

        $deskripsi = $request->deskripsi;
        $tugas = $request->tugas;
        $user = $request->user;
        $disposisiUser = new DisposisiUser;
        $disposisiUser->id_disposisi = $id_disposisi;
        $disposisiUser->id_user_support = $user;
        $disposisiUser->tugas = $tugas;
        $disposisiUser->keterangan = $deskripsi;
        $save = $disposisiUser->save();

        if ($save) {
            return [
            'status'=>'ok'
            ];
        }
    }

    public function getTask($id,Request $request){

        $data = Disposisi::where('id_request',$id);
        $id_disposisi = $data->first()->id_disposisi;
        $disposisiUser = DisposisiUser::where('id_disposisi',$id_disposisi);

        if ($disposisiUser->count() > 0 ) {
            $html = '';
            $no = 1;
            foreach ($disposisiUser->get() as $d) {
                $html .= '<tr>';
                $html .= '<td>'.$no++.'</td>';
                $html .= '<td>'.$d->user->nama.'</td>';
                $html .= '<td>'.$d->tugas.'</td>';
                $html .= '<td>'.$d->keterangan.'</td>';
                $html .= '<td><a class="btn btn-primary" href="'.url('task/'.$d->id_disposisi_user).'">Info</a> ';
                if(session('levelid')!=4){
                $html .='<button class="btn btn-danger delete" data-id="'.$d->id_disposisi_user.'">Hapus</button>';
                } 
                $html .= '</td>';
                $html .= '</tr>';

            }
            echo $html;
            # code...
        }

    }

    public function delete(Request $request){
        $id = $request->id;
        $find = DisposisiUser::find($id);

        if ($find->count() > 0) {
            $delete = $find->delete();
            return [
            'status'=>'ok',
            'msg'=>'Sukses Menghapus Data'
            ];
        }
        else{
         return [
            'status'=>'failed',
            'msg'=>'Gagal Menghapus Data'
            ];   
        }
    }


}
