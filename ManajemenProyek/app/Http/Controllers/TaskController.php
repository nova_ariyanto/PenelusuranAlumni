<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modul;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Redirect;
use Session;
use App\User;
use App\Disposisi;
use App\DisposisiUser;
use App\RequestData as Data;
use App\RequestModul;
use App\DisposisiUserWaktu;
use DB;
// use App\DisposisiUser;
class TaskController extends Controller
{

    private $folder = 'task';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



        public function __construct()
        {
            // parent::__construct();
            $this->middleware('login-auth');
        }

    public function index(){
        $user = session('userid');
        $data =DisposisiUser::where('id_user_support',$user)->paginate(10);
        return view($this->folder.'/index',compact('data'));
    }

    public function show($id){
        // echo $id;


        $data = DisposisiUser::find($id);
        if (count($data) == 0) {
            return abort(404);
        }
        else{

        return view($this->folder.'/show',compact('data'));
        }
    }

    public function start(Request $request,$id){
        // echo "string";
        $now = date('Y-m-d H:i:s');

        $user = session('userid');

        // if ($data->count() == 0) {
        $disposisiUserWaktu = new DisposisiUserWaktu;
        $disposisiUserWaktu->id_disposisi_user = $id;
        $disposisiUserWaktu->id_user = $user;
        $disposisiUserWaktu->waktu_mulai = $now;
        $disposisiUserWaktu->save();


//         $data = DB::select(DB::raw("SELECT CONCAT(
// FLOOR(HOUR(TIMEDIFF('2010-01-06 08:46', '2010-01-01 12:30')) / 24), ' days ',
// MOD(HOUR(TIMEDIFF('2010-01-06 08:46', '2010-01-01 12:30')), 24), ' hours ',
// MINUTE(TIMEDIFF('2010-01-06 08:46', '2010-01-01 12:30')), ' minutes') as second"));
        $data =  DisposisiUserWaktu::where('id_user',$user)->orderBy('id_disposisi_user_waktu','desc')->first()->waktu_mulai;


            $data = DB::select(DB::raw("SELECT UNIX_TIMESTAMP('".$data."') - UNIX_TIMESTAMP('".$now."') as output"));

        // return $data;

            return [
            'status'=>'ok',
            'second'=>$data[0]->output
            ];

        // }
        // else{
        // $data = DB::select(DB::raw("SELECT UNIX_TIMESTAMP('2010-11-29 13:16:55') - UNIX_TIMESTAMP('2010-11-29 13:13:55') as output"));

        // return $data;

        //     // return $data->first();
        // }

        // return $data;

    }

    public function stop($id,Request $request){
        $deskripsi = $request->deskripsi;
        $user = session('userid');
        $data =  DisposisiUserWaktu::where('id_user',$user)->orderBy('id_disposisi_user_waktu','desc')->first();
        $waktuBerakhir = $data->waktu_berakhir;
        $id = $data->id_disposisi_user_waktu;

        if ($waktuBerakhir == null) {
            $update = DisposisiUserWaktu::find($id);
            $update->waktu_berakhir = date('Y-m-d H:i:s');
            $update->keterangan =$deskripsi;
            $update->save();

            return [
            'status'=>'ok'
            ];
        }

    }

    public function fetch($id){
        $user = session('userid');
        $data =  DisposisiUserWaktu::where(['id_user'=>$user,'id_disposisi_user'=>$id])->orderBy('id_disposisi_user_waktu','desc');
        if ($data->count() > 0) {
            # code...
        $data = $data->first();
        $start = $data->waktu_mulai;
        $end = $data->waktu_berakhir;

        if ($end == null) {
        $now = date('Y-m-d H:i:s');
        $data = DB::select(DB::raw("SELECT UNIX_TIMESTAMP('".$now."') - UNIX_TIMESTAMP('".$start."') as output"));
        // return $data;
            return [
            'status'=>'ok',
            'second'=>$data[0]->output
            ];

        }
        else{
             return [
             'status'=>'null',
            'second'=>NULL
            ];
        }
        }
        else{
            return [
             'status'=>'null',
            'second'=>NULL
            ];
        }

    }

    public function activity($id){
        $user = session('userid');
        $data =  DisposisiUserWaktu::where(['id_user'=>$user,'id_disposisi_user'=>$id])->whereNotNull('waktu_berakhir')->orderBy('id_disposisi_user_waktu','desc');

        if ($data->count() >0) {
            $html = '';
            $no=1;
            foreach ($data->get() as $d) {
                $query = "SELECT CONCAT(MOD(HOUR(TIMEDIFF(waktu_mulai, waktu_berakhir)), 24), ' jam ') as waktu from tb_disposisi_user_waktu WHERE id_disposisi_user_waktu ='".$d->id_disposisi_user_waktu."'";
                $waktu = DB::select($query);
                // print_r($waktu);

                $html  .= '<tr>';
                $html  .= '<td>'.$no++.'</td>';
                $html  .= '<td>'.$d->keterangan.'</td>';
                $html  .= '<td>'.$d->waktu_mulai.'</td>';
                $html  .= '<td>'.$d->waktu_berakhir.'</td>';
                $html  .= '<td>'.$waktu[0]->waktu.'</td>';
                $html  .= '</tr>';
            }

            echo $html;
        }

    }

    public function updateStatus($id,Request $request){

        $status = $request->status;
        $user = session('userid');
        DisposisiUser::where(['id_user_support'=>$user,'id_disposisi_user'=>$id])->update(['status_pengerjaan'=>$status]);


        return redirect()->back();



    }



}
