<?php

namespace App\Http\Controllers;
use App\Kategori;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class KategoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $folder ='kategori';

   
   public function __construct()
   {
       // parent::__construct();
                    $this->middleware('login-auth');
   }
    public function data(){
        $kategori = Kategori::query();
            return Datatables::of($kategori)->addColumn('action',function($data){
                $html ='';
                $html .= '<a href="'.url('kategori/'.$data->id_kategori.'/edit').'" class="btn btn-success">Edit</a> ';
                $html .= '<form action="'.url('kategori/'.$data->id_kategori).'" method="post" style="display:inline">';
                $html .= csrf_field();
                $html .= method_field("DELETE");
                $html .= "<button class='btn btn-danger' type='submit'>Hapus</button>";
                $html .= "</form>";
                return $html;
            })
            ->make(true);
    }


    public function index()
    {
        //
        $data = Kategori::paginate(10);
        return view($this->folder.'/index',compact('data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view($this->folder.'/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = new Kategori;
        $data->nama_kategori = $request->nama_kategori;
        // $data->status = 0;
        $save = $data->save();
        if($save){
            $request->session()->flash('success','Berhasil Menyimpan Data');
        }else{
            $request->session()->flash('success','Gagal Menyimpan Data');
        }
        return redirect('kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data = Kategori::find($id);
        return view($this->folder.'/edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data  = Kategori::find($id);
        if(count($data)>0){
            $data->nama_kategori = $request->nama_kategori;
            $save = $data->save();
             if ($save) {
            $request->session()->flash('success','Berhasil Mengubah Data');
        }
        else{
            $request->session()->flash('danger','Gagal Mengubah Data');

        }
        return redirect('/kategori');
        }else{
           $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id ,Request $request)
    {
        //
        $data = Kategori::find($id);
        if (count($data) > 0) {
        $delete = $data->delete();
           if ($delete) {
            $request->session()->flash('success','Berhasil Menghapus Data');
        }
        else{
            $request->session()->flash('danger','Gagal Menghapus Data');

        }
        return redirect('/kategori');
        }else{

            $request->session()->flash('danger','Data Tidak Ditemukan');
            return redirect()->back();
        }

    }
}
