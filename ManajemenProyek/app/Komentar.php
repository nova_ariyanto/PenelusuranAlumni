<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    //
    protected $table= 'tb_komentar';
    protected $primaryKey ='id_komentar';
    public $timestamps = false;

    public function user(){
    	return $this->belongsTo('App\User','id_user');
    }
}
