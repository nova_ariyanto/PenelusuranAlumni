<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisposisiUserWaktu extends Model
{
    //


    protected $table= 'tb_disposisi_user_waktu';
    protected $primaryKey ='id_disposisi_user_waktu';
    public $timestamps = false;
    public $incrementing = false;

}
