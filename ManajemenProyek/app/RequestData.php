<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestData extends Model
{
    //

    protected $table= 'tb_request';
    protected $primaryKey ='id_request';
    public $timestamps = false;
 	public $incrementing = false;

 	public function kategori(){
 		return $this->belongsTo('App\Kategori','id_kategori');
 	}
 	public function user(){
 		return $this->belongsTo('App\User','id_user');

 	}
}
