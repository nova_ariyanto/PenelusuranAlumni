<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestModul extends Model
{
    //

    protected $table= 'tb_request_modul';
    protected $primaryKey ='id_request_modul';
	public $incrementing = false;
    public $timestamps = false;

    public function modul(){
    	return $this->belongsTo('App\Modul','id_modul');
    }
}
