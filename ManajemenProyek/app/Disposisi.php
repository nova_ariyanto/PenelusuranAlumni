<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disposisi extends Model
{
    //


    protected $table= 'tb_disposisi';
    protected $primaryKey ='id_disposisi';
    public $timestamps = false;
    public $incrementing = false;

    public function request(){

    	return $this->belongsTo('App\RequestData','id_request');
    }

}
