<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisposisiUser extends Model
{
    //


    protected $table= 'tb_disposisi_user';
    protected $primaryKey ='id_disposisi_user';
    public $timestamps = false;
    public $incrementing = false;
    public function user(){
    	return $this->belongsTo('App\User','id_user_support');
    }
    public function disposisi(){
    	return $this->belongsTo('App\Disposisi','id_disposisi');

    }

}
