@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Tambah Disposisi
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
				{{-- expr --}}
		
				{{csrf_field()}}	
				<div class="form-group">
					<label>ID</label>
					<input type="text" name="id" class="form-control" readonly value="{{$requestid}}" id="id">
				</div>
				<div class="form-group">
					<label>Tanggal Mulai</label>
					<input type="date" name="tanggal" class="form-control" id="tgl">
				</div>
				<div class="form-group">
					<label>Waktu Pengerjaan</label>
					<input type="number" name="waktu_pengerjaan" class="form-control"  id="waktu">
				</div>






			</div>
			<div class="col-lg-12">
				<h3>Task</h3>
				<button data-toggle="modal" data-target="#myModal" class="btn btn-primary">Tambah Task</button>
					<table class="table table-striped">
					<thead>
						<th>Nama User</th>
						<th>Tugas</th>
						<th>Deskripsi</th>
					</thead>
					<tbody id="tbody">
						
					</tbody>
					</table>
					<div id="save">
						
					</div>
			</div>
		

			</div>

		</div>
	</div>	


	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Task Baru</h4>
      </div>
      <div class="modal-body">
			<div class="form-group">
				<label>Tugas</label>
				<input type="text" name="" class="form-control" id="tugas">
			</div>
			<div class="form-group">
			<label>User</label>
			<br>
				<select class="form-control select" style="width: 100%;" id="user">
					<option value="">Pilih User</option>
					@foreach ($user as $u)
					<option value="{{$u->id_user}}">{{$u->nama}}</option>
					@endforeach

				</select>
			</div>
			<div class="form-group">
				<label>Deskripsi</label>
				<textarea id="deskripsi" class="form-control"></textarea>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="ok" >Ok</button>      
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	{{-- expr --}}
@endsection

@push('script')
<script>
	$(document).on('click','#ok',function(){
		var tugas = $("#tugas").val();
		var user = $("#user").val();
		var deskripsi = $("#deskripsi").val();

		var text = $("#user").children("option").filter(":selected").text()

		var html = "";
		html += "<tr>";
		html += "<td class='userid' data-user='"+user+"'>"+text+"</td>";
		html += "<td class='tugas'>"+tugas+"</td>";
		html += "<td class='deskripsi'>"+deskripsi+"</td>";
		html += "</tr>";
		$("#tbody").append(html);

		$("#tugas").val('');
		$("#user").val('');
		$("#deskripsi").val('');
		$("#myModal").modal('hide');
		$("#save").html('<button id="btn-save" class="btn btn-primary">Simpan</button>');
	});
	$(document).on('click','#btn-save',function(){
	var id 		= $("#id").val();
	var tgl 	= $("#tgl").val();
	var waktu 	= $("#waktu").val();
	// var users 
	var users;
	var tugas;
	var deskripsi;

	$("#tbody tr").each(function(){
		// var td = $()
		users += ','+$(this).find('.userid').attr('data-user');
		tugas += ','+$(this).find('.tugas').text();
		deskripsi += ','+$(this).find('.deskripsi').text();

		// alert($("td:first").html());
	});

	if(waktu.length == 0){
		alert("Waktu Tidak Boleh Kosong");
	}
	else{
		$.ajax({
			url: "{{ url('disposisi/create') }}",
			type: "POST",
			data: {
				id : id,
				tgl : tgl,
				waktu  : waktu,
				users :users,
				tugas :tugas,
				deskripsi :deskripsi,

			},
			dataType: "JSON",
			success: function(res){
				if(res.status == 'ok'){
					window.location.href = '{{ url('/request/') }}/'+res.id+'/disposisi';
				}
			}
		})
	}
	// console.log(waktu);

	});
</script>
	{{-- expr --}}
@endpush
