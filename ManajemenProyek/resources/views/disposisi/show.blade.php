@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Detail Disposisi
			</div>
			<div class="panel-body">


                @if ($data->count() != 0)
                 <div class="row">
                    <div class="col-lg-6">
                    <h3>Detail Data</h3>
                        <table class="table table-striped">
                            <tr>
                                <td>ID</td><td>{{$request->id_request}}</td>
                            </tr>
                            <tr>
                                <td>Nama Request</td><td>{{$request->nama_request}}</td>
                            </tr>
                            <tr>
                                <td>Kategori</td><td>{{$request->kategori->nama_kategori}}</td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td><td>{{$request->deskripsi}}</td>
                            </tr>
                            <tr>
                                <td>Pengirim</td><td>{{$request->user==null? 'User Tidak Ada' : $request->user->nama}}</td>
                            </tr>
                            <tr>
                                <td>Status</td>
                                <td>
                                    @if(session('levelid')==4)
                                    <span>
                                        {{$request->status==0 ? 'Ditinjau':''}}
                                        {{$request->status==1 ? 'Dikerjakan':''}}
                                        {{$request->status==2 ? 'Selesai':''}}
                                    </span>
                                    @else
                                        <form action="{{url('request/'.$request->id_request.'/status')}}" method="post">
                                        {{csrf_field()}}
                                        <select class="form-control" onchange="this.form.submit()" name="status">
                                        <option value="">Pilih Status</option>
                                        <option {{$request->status==0 ? 'selected':''}} value="0">Ditinjau</option>
                                        <option {{$request->status==1 ? 'selected':''}} value="1">Dikerjakan</option>
                                        <option {{$request->status==2 ? 'selected':''}} value="2">Selesai</option>
                                        </select>
                                        </form>    
                                    @endif

                                
                                </td>
                            </tr>
                        </table>
                        <h3>Disposisi</h3>
                        <table class="table table-striped">
                            
                            <tr>

                                <td>Tanggal Mulai</td><td>
                                    @if(session('levelid')==4)
                                    <span>
                                        {{$data->first()->tanggal_mulai}}
                                    </span>
                                    @else
                                    <input type="date" name="tanggal" value="{{$data->first()->tanggal_mulai}}" class="form-control" id="tanggal">
                                    @endif

                                </td>
                            </tr>
                            <tr>
                                <td>Waktu Pengerjaan</td><td>
                                    @if(session('levelid')==4)
                                    <span>
                                        {{$data->first()->waktu_deadline}}
                                    </span>
                                    @else
                                    <input type="number" value="{{$data->first()->waktu_deadline}}" name="waktu" class="form-control" id="waktu">
                                    @endif
                                </td>
                            </tr>
                        </table>
                        @php
                            $no=1;
                        @endphp

                    </div>
                    <div class="col-lg-6">
                        <h3>Daftar Modul</h3>
                            <table class="table table-striped">
                            @foreach ($modul->get() as $m)
                                {{-- expr --}}

                        @if ($m->modul !=null )
                            {{-- expr --}}
                                <tr>
                                    <td>{{$no++}}</td><td>{{$m->modul->nama}}</td>
                                </tr>
                        @endif

                            @endforeach
                        </table>
                    </div>

                    <div class="col-lg-12">
                    <h3>Task</h3>

                    
                                    @if(session('levelid')!=4)
                                        <button data-toggle="modal" data-target="#myModal" class="btn btn-primary"> Tambah Task</button>
                                    @endif


                        <table class="table table-striped">
                        <thead>
                            <th>#</th>
                            <th>User</th>
                            <th>Tugas</th>
                            <th>Deskripsi</th>
                        </thead>
                        <tbody id="tbody">
                        </tbody>
                        </table>
                    </div>
                @else
                <center><h3>Tidak Ada Data</h3></center>
                @endif
        </div>
			

		</div>
	</div>	
</div>



	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Task Baru</h4>
      </div>
      <div class="modal-body">
			<div class="form-group">
				<label>Tugas</label>
				<input type="text" name="" class="form-control" id="tugas">
			</div>
			<div class="form-group">
			<label>User</label>
			<br>
				<select class="form-control select" style="width: 100%;" id="user">
					<option value="">Pilih User</option>
					@foreach ($user as $u)
					<option value="{{$u->id_user}}">{{$u->nama}}</option>
					@endforeach

				</select>
			</div>
			<div class="form-group">
				<label>Deskripsi</label>
				<textarea id="deskripsi" class="form-control"></textarea>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="ok" >Ok</button>      
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


@endsection
@push('script')
<script type="text/javascript">
$(document).on('change','#waktu',function(){
	// ambil value 
	var waktu = $(this).val();
	$.ajax({
		url: "{{ url('/request/'.$request->id_request.'/disposisi/') }}",
		method: "POST",
		data: {
			val: waktu,
			type: "ubah-waktu"
		},
		success:function(res){
			if(res.status=='ok'){
	        $.notify("Waktu Pengerjaan Diubah");

			}
		}
	})
});
$(document).on('change','#tanggal',function(){
	// ambil value 
	var waktu = $(this).val();
	$.ajax({
		url: "{{ url('/request/'.$request->id_request.'/disposisi/') }}",
		method: "POST",
		data: {
			val: waktu,
			type: "ubah-tanggal"
		},
		success:function(res){
			if(res.status=='ok'){
	        $.notify("Tanggal Diubah");

			}
		}
	})
});

$(document).on('click','#ok',function(){
	var tugas = $("#tugas").val();
	var deskripsi = $("#deskripsi").val();
	var user = $("#user").val();

	$.ajax({
		url: "{{ url('/request/'.$request->id_request.'/disposisi/tambah-user') }}",
		method: "POST",
		data: {
			tugas: tugas,
			deskripsi: deskripsi,
			user: user
		},
		success:function(res){
			if(res.status=='ok'){
				$.notify("Berhasil Menambah User");
				$("#myModal").modal('hide');
				fetch();
			}
		}
	})
});
function fetch(){
	$.ajax({
		url: "{{ url('/request/'.$request->id_request.'/get') }}",
		method: "GET",
		success: function(res){
			$("#tbody").html(res);
		}
	})
}

$(document).on('click','.delete',function(){
	var id = $(this).attr('data-id');
	$.ajax({
		url: "{{ url('/disposisi/delete') }}",
		data: {
			id: id
		},
		method: "POST",
		success: function(res){
			// if(res.status == 'ok')

	        $.notify(res.msg);
	        fetch();
		}
	})
});
$(document).ready(function(){
	fetch();
})

</script>
	{{-- expr --}}
@endpush