@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar Modul
			</div>
			<div class="panel-body">
			@php
				$no=1;
			@endphp
			<a href="{{ url('/modul/create') }}" class="btn btn-primary">Tambah</a>
				<table class="table table-striped" id="table">
					<thead>
						<th>#</th>
						<th>Modul</th>
						<th>Opsi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>	


	@push('script')
	<script type="text/javascript">
		
$(function() {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{url(Request::segment(1).'/data')}}',
        columns: [
            { data: 'id_modul', name: 'id_modul' },
            { data: 'nama', name: 'nama' },
            { data: 'action', name: 'action' },
        ]
    });
});

	</script>
	@endpush
	{{-- expr --}}
@endsection