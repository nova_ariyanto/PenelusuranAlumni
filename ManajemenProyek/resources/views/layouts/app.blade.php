<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->

    <link href="//cdnjs.cloudflare.com/ajax/libs/animate.css/3.2.0/animate.min.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://fontawesome.com/v4.7.0/assets/font-awesome/css/font-awesome.css">


<meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->

                        @if (empty(session('userid')))
                            {{-- expr --}}
                            <li><a href="{{ url('login') }}">Login</a></li>
                            <li><a href="{{ url('register') }}">Register</a></li>
                        @else

                            @if(session('levelid')==1)
                            <!-- menu untuk admin -->   
                            <li><a href="{{ url('modul') }}">Modul</a></li>
                            <li><a href="{{ url('level') }}">Level</a></li>
                            <li><a href="{{ url('user') }}">User</a></li>
                            <li><a href="{{ url('kategori') }}">Kategori</a></li>
                            <li><a href="{{ url('request') }}">Request/Tiket</a></li>
                            @elseif(session('levelid')==2)
                            <!-- menu untuk it -->
                            <li><a href="{{ url('request') }}">Request/Tiket</a></li>
                             <li><a href="{{ url('user') }}">User</a></li>
                            @elseif(session('levelid')==3)
                            <!-- menu untuk pegawai rumah sakit -->
                            <li><a href="{{ url('request') }}">Request Saya</a></li>
                            @elseif(session('levelid')==4)
                            <!-- menu untuk pic -->
                            <li><a href="{{ url('task') }}">Pekerjaan Saya</a></li>
                            @endif

                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ session('username') }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                        
                    </ul>
                </div>
            </div>
        </nav>

    @if (session('success'))
        <div class="container">
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        </div>
    @endif

    @if (session('danger'))
        <div class="container">
            <div class="alert alert-danger">
                {{session('danger')}}
            </div>
        </div>
    @endif
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>


    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/highlight.min.js"></script>
<script src="http://bootstrap-notify.remabledesigns.com/js/bootstrap-notify.min.js"></script>
<script>
    $(document).ready(function() {
    $('.select').select2();
});
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

    @stack('script')

</body>
</html>
