@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar kategori
			</div>
			<div class="panel-body">
			@php
				$no=1;
			@endphp
			<a href="{{ url('/kategori/create') }}" class="btn btn-primary">Tambah</a>
				<table class="table table-striped" id="table">
					<thead>
						<th>#</th>
						<th>kategori</th>
						<th>Opsi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>	


	@push('script')
	<script type="text/javascript">
		
$(function() {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{url(Request::segment(1).'/data')}}',
        columns: [
            { data: 'id_kategori', name: 'id_kategori' },
            { data: 'nama_kategori', name: 'nama_kategori' },
            { data: 'action', name: 'action' },
        ]
    });
});

	</script>
	@endpush
	{{-- expr --}}
@endsection