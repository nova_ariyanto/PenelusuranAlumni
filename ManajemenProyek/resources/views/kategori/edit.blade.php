@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Ubah Kategori
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
			@if ($data->count() != 0)
				{{-- expr --}}
				<form action="{{ url('kategori/'.$data->id_kategori) }}" method="post">

				{{csrf_field()}}	
				{{method_field("PUT")}}
				<div class="form-group">
					<label>Nama Kategori</label>
					<input type="text" name="nama_kategori" class="form-control" value="{{$data->nama_kategori}}">
				</div>
				<button type="submit" class="btn btn-primary">Ubah</button>
				</form>
			</div>
			@else
			<center><h3>Data Tidak Ditemukan</h3></center>
			@endif
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection