@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Ubah Level
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
			@if ($data->count() != 0)
				{{-- expr --}}
				<form action="{{ url('level/'.$data->id_level) }}" method="post">

				{{csrf_field()}}	
				{{method_field("PUT")}}
				<div class="form-group">
					<label>Nama Level</label>
					<input type="text" name="nama" class="form-control" value="{{$data->nama}}">
				</div>
				<button type="submit" class="btn btn-primary">Ubah</button>
				</form>
			</div>
			@else
			<center><h3>Data Tidak Ditemukan</h3></center>
			@endif
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection