@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Tambah Level
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
				{{-- expr --}}
				<form action="{{ url('level/') }}" method="post">

				{{csrf_field()}}	
				<div class="form-group">
					<label>Nama Level</label>
					<input type="text" name="nama" class="form-control" required>
				</div>
				<button type="submit" class="btn btn-primary">Simpan</button>
				</form>
			</div>
	
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection