@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Ubah Modul
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
			@if ($data->count() != 0)
				{{-- expr --}}
				<form action="{{ url('request/'.$data->id_request) }}" method="post">

				{{csrf_field()}}	
				{{method_field("PUT")}}
				<div class="form-group">
					<label>Nama </label>
					<input type="text" name="nama" class="form-control" required value="{{$data->nama_request}}">
				</div>
				<div class="form-group">
					<label>Modul </label>
				<select class="form-control select" name="modul[]" multiple>

					@foreach ($array as $a)
					<option {{$a['state']}} value="{{$a['id']}}">{{$a['modul']}}</option>
					@endforeach
				</select>
				</div>
				<div class="form-group">
					<label>Jenis</label>
				<select class="form-control select" name="kategori">
					@foreach ($kategori as $k)
					<option {{$data->id_kategori==$k->id_kategori ? 'selected':''}} value="{{$k->id_kategori}}">{{$k->nama_kategori}}</option>
					@endforeach
				</select>
				</div>
				<div class="form-group">
					<label>Deskripsi</label>
					<textarea name="deskripsi" class="form-control">{{$data->deskripsi}}</textarea>
				</div>

				<button type="submit" class="btn btn-primary">Ubah</button>
				</form>
			</div>
			@else
			<center><h3>Data Tidak Ditemukan</h3></center>
			@endif
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection