@extends('layouts.app')

@section('content')

@if (session('success'))
        <div class="container">
            <div class="alert alert-success">
                {{session('success')}}
            </div>
        </div>
    @endif


{{Session::get('danger')}}
    @if (session('danger'))
        <div class="container">
            <div class="alert alert-danger">
                {{session('danger')}}
            </div>
        </div>
    @endif

    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                #{{$data->id_request}}
            </div>
            <div class="panel-body">
                <div class="pull-right">
                    @if($disposisi == 0)
                    @if(session('levelid') <= 2  )
                    @if ($data->status == 0 )
                        {{-- expr --}}
                    <a href="{{url('/disposisi/create/'.$data->id_request) }}" class="btn btn-primary">Disposisi</a>
                    @endif
                    @endif
                    @else
                    <a class="btn btn-info" href="{{url('request/'.$data->id_request.'/disposisi')}}">Info</a>
                    @endif
                    <br>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                    <h3>Detail Data</h3>
                        <table class="table table-striped">
                            <tr>
                                <td>ID</td><td>{{$data->id_request}}</td>
                            </tr>
                            <tr>
                                <td>Nama Request</td><td>{{$data->nama_request}}</td>
                            </tr>
                            <tr>
                                <td>Kategori</td><td>{{$data->kategori->nama_kategori}}</td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td><td>{{$data->deskripsi}}</td>
                            </tr>
                            <tr>
                                <td>Pengirim</td><td>{{$data->user==null? 'User Tidak Ada' : $data->user->nama}}</td>
                            </tr>
                            <tr>
                                <td>Status</td><td>
                                    @if(session('levelid')==3)
                                    {{$data->status==0 ? 'Ditinjau':''}}
                                    {{$data->status==1 ? 'Dikerjakan':''}}
                                    {{$data->status==2 ? 'Selesai':''}}
                                    @else                                    
                                <form action="{{url('request/'.$data->id_request.'/status')}}" method="post">
                                {{csrf_field()}}
                        <select class="form-control" onchange="this.form.submit()" name="status">
                            <option value="">Pilih Status</option>
                            <option {{$data->status==0 ? 'selected':''}} value="0">Ditinjau</option>
                            <option {{$data->status==1 ? 'selected':''}} value="1">Dikerjakan</option>
                            <option {{$data->status==2 ? 'selected':''}} value="2">Selesai</option>
                        </select>
                        </form>
                                @endif
                            </tr>
                        </table>
                        @php
                            $no=1;
                        @endphp

                    </div>
                    <div class="col-lg-6">
                        <h3>Daftar Modul</h3>
                                                <table class="table table-striped">
                            @foreach ($modul->get() as $m)
                                {{-- expr --}}

                        @if ($m->modul !=null )
                            {{-- expr --}}
                                <tr>
                                    <td>{{$no++}}</td><td>{{$m->modul->nama}}</td>
                                </tr>
                        @endif

                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                Komentar
            </div>
            <div class="panel-body">
                <ul class="list-group">
                @if ($komentar->count() > 0)
                @foreach ($komentar->get() as $k)
                    {{-- expr --}}
                    <li class="list-group-item">
                        <div class="pull-right">
{{--                             <a href="#" class="btn btn-danger btn-xs">Hapus</a> --}}


@if (session('userid') == $k->id_user)
    {{-- expr --}}
                            <form action="{{ url('komentar/'.$k->id_komentar) }}" style="display: inline;" method="post">
                                {{method_field("DELETE")}}
                                {{csrf_field()}}
                                <button class="btn btn-xs btn-danger">Hapus</button>
                                
                            </form>
@endif
                        </div>
                        <a title="" href="#">{{$k->user->username}}</a> <br>
                        {{$k->komentar}}
                    </li>
                @endforeach
                @else
                <center><h3>Belum Ada Komentar </h3></center>
                @endif

                </ul>
                <form action="{{ url('komentar/'.$data->id_request) }}" method="post">
                {{csrf_field()}}
                    
                <div class="form-group">

                    <textarea name="komentar" class="form-control"></textarea>
                                        <br>
                    <button type="submit" class="btn btn-primary">Komentar</button>

                </div>
                </form>
            </div>
        </div>
    </div>  

    {{-- expr --}}
@endsection