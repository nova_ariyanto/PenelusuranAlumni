@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar Request/Tiket
			</div>
			<div class="panel-body">
			@php
				$no=1;
			@endphp
			<a href="{{ url('/request/create') }}" class="btn btn-primary">Tambah</a>
				<table class="table table-striped" id="table">
					<thead>
						<th>#</th>
						<th>Request </th>
						<th>Status</th>
						<th>Opsi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>	


	@push('script')
	<script type="text/javascript">
		
$(function() {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{url(Request::segment(1).'/data')}}',
        columns: [
            { data: 'id_request', name: 'id_request' },
            { data: 'nama_request', name: 'nama_request' },

            { data: 'status', name: 'status',searchable:true },
            { data: 'action', name: 'action' },
        ]
    });
});

	</script>
	@endpush
	{{-- expr --}}
@endsection