@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar Request/Tiket
			</div>
			<div class="panel-body">
			@php
				$no=1;
			@endphp
			<a href="{{ url('/request/create') }}" class="btn btn-primary">Tambah</a>
			<br>
			<br>
			<ul class="list-group"> 
			@foreach ($data as $d)
				{{-- expr --}}
				<li class="list-group-item">
					<a href="{{ url('/request/'.$d->id_request) }}">
						{{$d->nama_request}}
					</a> <br>
						{{$d->deskripsi}} <br>
						@if($d->status==0)
						<span class="btn-danger btn btn-xs ">Ditinjau</span>
						@elseif($d->status==1)
						<span class="btn btn-danger">Dikerjakan</span>
						@elseif($d->status==2)
						<span class="btn-primary btn btn-xs">Selesai</span>
						@endif

				</li>

			@endforeach
			</ul>
			</div>
		</div>
	</div>	

@endsection