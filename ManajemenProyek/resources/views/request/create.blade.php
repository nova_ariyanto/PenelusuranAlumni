@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Tambah Request/Tiket
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
				{{-- expr --}}
				<form action="{{ url('request/') }}" method="post">

				{{csrf_field()}}	
				<div class="form-group">
					<label>Nama </label>
					<input type="text" name="nama" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Modul </label>
				<select class="form-control select" name="modul[]" multiple>

					@foreach ($modul as $m)
					<option value="{{$m->id_modul}}">{{$m->nama}}</option>
					@endforeach
				</select>
				</div>
				<div class="form-group">
					<label>Jenis</label>
				<select class="form-control select" name="kategori">
					@foreach ($kategori as $k)
					<option value="{{$k->id_kategori}}">{{$k->nama_kategori}}</option>
					@endforeach
				</select>
				</div>
				<div class="form-group">
					<label>Deskripsi</label>
					<textarea name="deskripsi" class="form-control"></textarea>
				</div>
				<button type="submit" class="btn btn-primary">Simpan</button>
				</form>
			</div>
	
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection
