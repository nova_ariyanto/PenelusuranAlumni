@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Tambah User
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
				{{-- expr --}}
				<form action="{{ url('user/') }}" method="post">

				{{csrf_field()}}	
				<div class="form-group">
					<label>Nama User</label>
					<input type="text" name="nama" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" name="password" class="form-control" required>
				</div>
				<div class="form-group">
					<label>Level</label>
					 <select class="form-control" name='id_level' required="">
					 	<option value="">Silahkan pilih level</option>
					 	@foreach($level as $l)
					 	<option value="{{$l->id_level}}">{{$l->nama}}</option>
					 	@endforeach
					 </select>
                     </div>
				<button type="submit" class="btn btn-primary">Simpan</button>
				</form>
			</div>
	
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection