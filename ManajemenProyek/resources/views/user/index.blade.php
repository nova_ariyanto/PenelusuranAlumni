@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar User 
			</div>
			<div class="panel-body">
			@php
				$no=1;
			@endphp
			<a href="{{ url('/user/create') }}" class="btn btn-primary">Tambah</a>
				<table class="table table-striped" id="table">
					<thead>
						<th>#</th>
						<th>Nama</th>
						<th>Level</th>
						<th>Username</th>
						<th>Opsi</th>
					</thead>
				</table>
			</div>
		</div>
	</div>	


	@push('script')
	<script type="text/javascript">
		
$(function() {
    $('#table').DataTable({
        processing: true,	
        serverSide: true,
        ajax: '{{url(Request::segment(1).'/data')}}',
        columns: [
            { data: 'id_user', name: 'id_user' },
            { data: 'nama', name: 'nama' },
            { data: 'Level', name: 'Level' },
            { data: 'username', name: 'username' },
            { data: 'action', name: 'action' },
        ]
    });
});

	</script>
	@endpush
	{{-- expr --}}
@endsection