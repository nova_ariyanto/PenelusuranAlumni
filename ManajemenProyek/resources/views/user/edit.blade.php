@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Ubah Modul
			</div>
			<div class="panel-body">
			<div class="col-lg-4">
			@if ($data->count() != 0)
				{{-- expr --}}
				<form action="{{ url('user/'.$data->id_user) }}" method="post">

				{{csrf_field()}}	
				{{method_field("PUT")}}
				<div class="form-group">
					<label>Nama User</label>
					<input type="text" name="nama" class="form-control" value="{{$data->nama}}">
				</div>
					<div class="form-group">
					<label>Username</label>
					<input type="text" name="username" class="form-control" value="{{$data->username}}">
				</div>
					<div class="form-group">
					<label>Password</label>
					<input type="text" name="password" class="form-control" value="">
				</div>
					<div class="form-group">
					<label>Level</label>
					<select class="form-control" name="id_level">

						<option>Silahkan pilih level</option>
						@foreach($level as $l)
					 	<option 
					 	{{
					 		$data->id_level==$l->id_level ? 'selected' : ''}}
					 	 value="{{$l->id_level}}">{{$l->nama}}</option>
					 	@endforeach

					</select>
				</div>

				<button type="submit" class="btn btn-primary">Ubah</button>
				</form>
			</div>
			@else
			<center><h3>Data Tidak Ditemukan</h3></center>
			@endif
			</div>
		</div>
	</div>	
	{{-- expr --}}
@endsection