@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Daftar Pekerjaan Saya
			</div>
			<div class="panel-body">
			@php
				$no=1;
			@endphp
	<ul class="list-group"> 
	@if ($data->count() >0)
		{{-- expr --}}
	@foreach ($data as $d)
		{{-- expr --}}
		<li class="list-group-item">
		<span>
		<a href="{{url('task/'.$d->id_disposisi_user)}}">
			<i class="fa fa-tasks"></i>
			{{$d->tugas}} 
		</span>
		</a>
		<br>
			<span title="Nama">
	<i class="fa fa-ticket-alt"></i>		{{$d->disposisi->request->nama_request}} <br>
			</span>
			<span title="Nama">
	<i class="fa fa-calendar-alt"></i>		{{$d->disposisi->tanggal_mulai}} <br>
			</span>
			<span title="Nama">
	<i class="fa fa-clock"></i>		{{$d->disposisi->waktu_deadline}} Hari <br>
			</span>
			<span title="Deskripsi">
			<i class="fa fa-info"></i>				{{$d->disposisi->request->deskripsi}} <br>
			</span>
			{!!$d->status_pengerjaan == 0 ? '<label class="label label-danger">Belum Dikerjakan</label>':($d->status_pengerjaan == 1 ? '<label class="label label-warning">Sedang Dikerjakan</label>':'<label class="label label-success">Sudah Dikerjakan</label>')!!}
		</li>
	@endforeach
	@else
	<li class="list-group-item">Tidak Ada Data</li>
	@endif
	</ul>
	<center>{{$data->links()}}</center>
			</div>
		</div>
	</div>	

{{-- 
	@push('script')
	<script type="text/javascript">
		
$(function() {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{url(Request::segment(1).'/data')}}',
        columns: [
            { data: 'id_request', name: 'id_request' },
            { data: 'nama_request', name: 'nama_request' },
            { data: 'action', name: 'action' },
        ]
    });
});

	</script>
	@endpush --}}
	{{-- expr --}}
@endsection