@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="panel panel-default">
			<div class="panel-heading">
				Task Detail
			</div>
			<div class="panel-body">
			<a class="btn btn-primary" href="{{ url('/request/'.$data->disposisi->request->id_request.'/disposisi') }}">Info</a>
			<div class="row">
				<div class="col-lg-6">
					<h3>Disposisi</h3>
					<table class="table table-hover">
						<tr>
							<td>Tanggal Mulai</td><td>{{$data->disposisi->tanggal_mulai}}</td>
						</tr>
						<tr>
							<td>Waktu Pengerjaan</td><td>{{$data->disposisi->waktu_deadline.' Hari'}}</td>
						</tr>
						<tr>
							<td>Status Pengerjaan</td><td>
								<form action="{{ url('/task/'.Request::segment(2).'/update-status') }}" method="post">
									{{csrf_field()}}
									<select class="form-control" name="status" onchange="this.form.submit()">
										<option {{$data->status_pengerjaan == 0 ? 'selected':''}} value="0">Belum Dikerjakan</option>
										<option {{$data->status_pengerjaan == 1 ? 'selected':''}} value="1">Sedang Pekerjaan</option>
										<option {{$data->status_pengerjaan == 2 ? 'selected':''}} value="2">Selesai</option>
									</select>
								</form>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-lg-6">
					
					<h3>Info</h3>
					<table class="table table-hover">
						<tr>
							<td>Permintaan</td><td>{{$data->disposisi->request->nama_request}}</td>
						</tr>
						<tr>
							<td>Deskripsi</td><td>{{$data->disposisi->request->deskripsi}}</td>
						</tr>
						
					</table>
				</div>
				
			</div>
	
			</div>
		</div>
			{{-- expr --}}
		@if ($data->id_user_support == session('userid'))
			{{-- expr --}}
			<div class="panel panel-default">

			<div class="panel-body">
				
		@if ($data->status_pengerjaan == 1)
					<h3>Timer</h3>
				<div id="wrap">
					

				<center><h3 id="state">Klik Untuk Memulai Pekerjaan</h3></center>	
				<div id="btn-state">
					
				</div>

				<br>
				<div class="col-lg-6" style="float: none;margin: 0 auto;display: none;" id="upload-wrap">
				<center><span style="font-size: 20px;" id="timer"></span></center>

				<center><span class="timer" style="font-size: 20px;" ></span></center>
					<textarea class="form-control deskripsi" placeholder="Apa Yang Anda Kerjakan?"></textarea>
					<br>
					<button class="btn btn-danger btn-stop">Berhenti</button>
				</div>
				</div>
		@else
		<center><h3>Ubah Status Pekerjaan untuk Memulai...</h3></center>
		@endif
	
			</div>
		</div>
		@endif
		<div class="panel panel-default">
			<div class="panel-body">
			<h3>Daftar Aktivitas</h3>
				<table class="table table-striped">
					<thead>
						<th>#</th>
						<th>Keterangan</th>
						<th>Waktu Mulai</th>
						<th>Waktu Berakhir</th>
						<th>Total Waktu</th>
					</thead>
					<tbody id="activity">
						
					</tbody>
				</table>
			</div>
		</div>
		

	</div>	
	{{-- expr --}}
@endsection

@push('script')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/timer.jquery/0.7.0/timer.jquery.js"></script>
    <script type="text/javascript">
    	   

    </script>
    <script>
    	$(document).on('click','.btn-play',function(){

    		$.ajax({
    			url:"{{ url('task/'.Request::segment(2)).'/start' }}",
    			method: "POST",
    			success:function(res){
    				if(res.status=='ok'){
    					$("#timer").hide();
  			$(".timer").timer({
            seconds: 0
            });
    		$(".btn-play").hide();
    		$("#upload-wrap").show();
    		$("#state").hide();

// alert("as");

    				}
    			}
    		})
    	});

    	    	$(document).on('click','.btn-stop',function(){
    	    		var deskripsi  = $(".deskripsi").val();
    		$.ajax({
    			url:"{{ url('task/'.Request::segment(2)).'/stop' }}",
    			data: {
    				deskripsi: deskripsi
    			},
    			method: "POST",
    			success:function(res){
    				if(res.status=='ok'){

    					$("#btn-state").html('<center><button class="btn btn-success btn-lg btn-play">Mulai</button></center>');
    		$("#upload-wrap").hide();
    		$("#timer").hide();
    		$("#state").show();
    		$(".deskripsi").val('');
    		activity();
    				}
    			}
    		})
    	});
    	    	fetch();
    	    function fetch(){
    	    	$.ajax({
    	    		url: "{{ url('task/'.Request::segment(2).'/get') }}",
    	    		method: "GET",
    	    		success: function(res){
    	    			// console.log(res);
    	    			if(res.status=='ok'){

    		$(".btn-play").hide();
    		$("#upload-wrap").show();
    		$("#state").hide();

  			$("#timer").timer({
            seconds: res.second
            });


    	    			}
    	    			else{
    	    				$("#btn-state").html('<center><button class="btn btn-success btn-lg btn-play">Mulai</button></center>');
    	    			}

    	    		}
    	    	});
    	    }

    	    activity();
    	    function activity(){
    	    	$.ajax({
    	    		url: "{{ url('task/'.Request::segment(2).'/activity') }}",
    	    		method: "GET",
    	    		success: function(res){
    	    			// console.log(res);
    	    			$("#activity").html(res);

    	    		}
    	    	});


    	    }
    </script>
	{{-- expr --}}
@endpush