<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(['middlewareGroups' => ['web']], function () {

// });


Route::group(['middleware' => ['web']], function () {

Route::get('/', 'HomeController@index');

Route::get('/test', function () {
    return view('theme/index');
});
Route::get('/login', "AuthController@login")->name('login');
Route::post('/login', "AuthController@loginPost");
Route::post('/logout', "AuthController@logoutPost");

// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/modul/data', 'ModulController@data');
Route::resource('modul', 'ModulController');

Route::get('/user/data','UserController@data');
Route::resource('/user','UserController');

Route::get('/kategori/data', 'KategoriController@data');
Route::resource('kategori', 'KategoriController');

Route::get('/level/data', 'LevelController@data');
Route::resource('level', 'LevelController');

Route::get('/request/data', 'RequestController@data');
// ubah status
Route::post('request/{id}/status', 'RequestController@change');
Route::resource('request', 'RequestController');
Route::post('/komentar/{request}','KomentarController@save');
Route::delete('komentar/{id}', 'KomentarController@delete');
Route::get('disposisi/create/{request}','DisposisiController@create');
Route::post('disposisi/create','DisposisiController@save');
Route::get('request/{request}/disposisi','DisposisiController@show');
Route::get('request/{request}/get','DisposisiController@getTask');
Route::post('request/{request}/disposisi','DisposisiController@post');
Route::post('request/{request}/disposisi/tambah-user','DisposisiController@addUser');
Route::get('task/', 'TaskController@index');
Route::get('task/{id}', 'TaskController@show');
Route::post('task/{id}/start', 'TaskController@start');
Route::post('task/{id}/stop', 'TaskController@stop');
Route::get('task/{id}/get', 'TaskController@fetch');;
Route::get('task/{id}/activity', 'TaskController@activity');
Route::post('disposisi/delete', 'DisposisiController@delete');
Route::post('task/{id}/update-status','TaskController@updateStatus');
});